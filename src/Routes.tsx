import {Route, Switch, withRouter, RouteComponentProps, Redirect} from 'react-router-dom'

import React from 'react';
import UploadPage from './pages/UploadPage';
import LandingPage from './pages/LandingPage';

const Routes=(props: RouteComponentProps)=> {
    return (
        <Switch>
            <Route exact path="/" component={LandingPage} />
            <Route exact path="/upload" component={UploadPage} />
            <Redirect  to="/" />
        </Switch>
    );
}

export default withRouter(Routes);