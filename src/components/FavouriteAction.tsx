import React from 'react';
import {Tooltip} from 'antd'
import {HeartOutlined, HeartTwoTone} from "@ant-design/icons";
import {Image} from "../types";

interface Props {
    image:Image
    addToFavourite: (image:Image)=>void
    removeFromFavourite: (image:Image)=>void
}

export const FavouriteAction = ({image,addToFavourite,removeFromFavourite}:Props) =>  {

    const favourite_id = image?.favourite?.id

    return (
        <Tooltip title={favourite_id ? 'Remove Favourite' : 'Add Favourite'} >
            {favourite_id ?
                <HeartTwoTone onClick={()=> {removeFromFavourite(image)}} twoToneColor="#eb2f96" /> :
                <HeartOutlined onClick={()=> {addToFavourite(image)}}/>
            }
        </Tooltip>
    )
}