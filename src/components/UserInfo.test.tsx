import React from 'react';
import { render, screen ,waitFor} from '../integration-tests';
import {UserInfo} from './';


describe('<Navbar/>', () => {
    it('should render sign in on anonymous user', async () => {
        render(<UserInfo />,{
               preloadedState: {user: {name:'anonymous', email:'anonymous'}}}
                )
        await waitFor(() =>{
            expect(screen.queryByTestId('ButtonSignIn')).toBeInTheDocument()
            expect(screen.queryByTestId('ButtonSignOut')).not.toBeInTheDocument()
            }
        )
    });


    it('should render sign out if user is logged in', async () => {
        render(<UserInfo />,{
                preloadedState: {user: {name:'test', email:'test@test.com'}}}
        )
        await waitFor(() =>{
                expect(screen.queryByTestId('ButtonSignIn')).not.toBeInTheDocument()
                expect(screen.queryByTestId('ButtonSignOut')).toBeInTheDocument()
            }
        )
    });

})