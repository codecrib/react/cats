import React from 'react';
import {Menu} from 'antd';
import { useHistory,useLocation} from "react-router-dom";

import {ErrorBoundary, UserInfo} from "./"


export const Navbar= ()=> {
    const history = useHistory();
    const location = useLocation();
    const currentPath = location.pathname.slice(1) || 'home'
    return (
        <>
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['home']} selectedKeys={[currentPath]}  >
                <Menu.Item data-testid={'home'} key={'home'} onClick={()=>{history.push(  `/`)}}>{`Home`}</Menu.Item>
                <Menu.Item data-testid={'upload'} key={'upload'} onClick={()=>{history.push(`/upload`)}} >{`Upload`}</Menu.Item>
            </Menu>
            <div style={{position: 'absolute', top: 0, right: 32}} data-testid={'user-info'}>
                <ErrorBoundary>
                    <UserInfo/>
                </ErrorBoundary>
            </div>
        </>
    )
}

