import React from 'react';
import {Avatar, Button, Space, Typography} from "antd";
import {GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline, GoogleLogout} from "react-google-login";
import {LogoutOutlined} from "@ant-design/icons";
import {Dispatch} from "redux";
import {useDispatch, useSelector} from "react-redux";
import { loginSuccessResponse, loginFailureResponse, logoutSuccessResponse} from "../store/actionCreators";
import {RootState} from "../store";
import {pick} from "lodash";
import styled from "styled-components";
import {User} from "../types";
import config from "../config";
const { Title } = Typography;

const HideOnMobile = styled.div`
  @media (max-width: 768px) {
    display:none;
  }
`

export const UserInfo = () =>  {
    const dispatch: Dispatch<any> = useDispatch()
    const user:User = useSelector((state: RootState) => state.user)

    const loginSuccess = (response: GoogleLoginResponse | GoogleLoginResponseOffline ) => {
        "profileObj" in response ?
            dispatch(loginSuccessResponse( pick(response.profileObj,['name','email','imageUrl']))) :
            console.log(response);
    }

    const loginFailure = (error: any  ) => {
            dispatch(loginFailureResponse())
    }

    const logout = ( ) => {
        dispatch(logoutSuccessResponse())
    }

    return (
        <>
        {user && user?.email !== 'anonymous'? (
            <Space direction="horizontal" align="baseline" >
                    <Avatar src={user.imageUrl}/>
                   <HideOnMobile>
                       <Title style={{color:"snow"}} level={5}>{user.name}</Title>
                   </HideOnMobile>
                    <GoogleLogout
                        clientId={config.GOOGLE_CLIENT_ID}
                        render={renderProps => (
                            <Button type="primary" data-testid={'ButtonSignOut'} icon={<LogoutOutlined />} shape="circle" onClick={renderProps.onClick} disabled={renderProps.disabled} />
                        )}
                          onLogoutSuccess={logout}
                    >
                    </GoogleLogout></Space>
            )
                :
                (<GoogleLogin
                    clientId={config.GOOGLE_CLIENT_ID}
                    render={renderProps => (
                        <Button type="primary" data-testid={'ButtonSignIn'}  onClick={renderProps.onClick} disabled={renderProps.disabled}>Sign in</Button>
                    )}
                    onSuccess={loginSuccess}
                    onFailure={loginFailure}
                    isSignedIn={true}
                    cookiePolicy={'single_host_origin'}
                />)}
        </>
    )
}

