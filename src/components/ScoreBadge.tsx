import React, { PropsWithChildren} from 'react';
import {Badge} from 'antd'
import { useSelector} from "react-redux";
import {RootState} from "../store";

interface Props {
    image_id:string
}

export const ScoreBadge = ({image_id, children}:PropsWithChildren<Props>) =>  {
    const score:number = useSelector((state: RootState) => state.scores.find((x:any)=> x.image_id===image_id)?.score || 0)

    return (
        <Badge.Ribbon text={`Score : ${score}`}>
            {children}
        </Badge.Ribbon>
    )
}

