import React from 'react';
import { render, screen ,waitFor} from '../integration-tests';
import {ScoreBadge} from './';



describe('<Navbar/>', () => {
    it('should render score correctly', async () => {
        render(<ScoreBadge image_id={'test'} >
            <div>test children</div>
            </ScoreBadge>,{
               preloadedState: {scores: [{image_id:'test', score:5}]}}
                )
        await waitFor(() =>{
            expect(screen.queryByText('Score : 5')).toBeInTheDocument()
            }
        )
    });

    it('should render score 0 for unpresent image', async () => {
        render(<ScoreBadge image_id={'unpresent_image_id'}>
                <div>test children</div>
            </ScoreBadge>,{
                preloadedState: {scores: [{image_id:'test', score:5}]}}
        )
        await waitFor(() =>{
                expect(screen.queryByText('Score : 0')).toBeInTheDocument()
            }
        )
    });

})