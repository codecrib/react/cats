import React from 'react';
import { StyledLayout } from './Layout.styles';
import {Navbar} from "./";

const { Header, Footer, Content } = StyledLayout;
type Props = React.PropsWithChildren<{}>;

export const MainLayout = ({children}:Props) =>  {
    return (
            <StyledLayout>
                <Header> <Navbar/> </Header>
                <Content>
                        {children}
                 </Content>
                <Footer>Footer</Footer>
            </StyledLayout>
    );
}