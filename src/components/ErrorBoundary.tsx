import  { Component, ErrorInfo, ReactNode } from "react";
import {message} from "antd"

interface Props {
    children: ReactNode;
}

interface State {
    hasError: boolean;
    error: Error|null;
}

export class ErrorBoundary extends Component<Props, State> {
    public state: State = {
        hasError: false,
        error:null
    };

    public static getDerivedStateFromError(error: Error): State {
        return { hasError: true, error};
    }

    public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        this.setState({ hasError:true, error });
        message.error(error.message)
    }

    public render() {
        return this.props.children;
    }
}

