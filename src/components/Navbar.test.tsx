import React from 'react';
import {MemoryRouter,Route, RouteComponentProps } from 'react-router-dom';
import userEvent from "@testing-library/user-event";

import { render, screen ,waitFor} from '../integration-tests';
import {Navbar} from './';


describe('<Navbar/>', () => {
    it('should render', async () => {
        render(
                <MemoryRouter>
                    <Navbar />
                </MemoryRouter>
            )
        await waitFor(() =>{
            expect(screen.getByTestId('home')).toBeInTheDocument()
            expect(screen.getByTestId('upload')).toBeInTheDocument()
            }
        )
    });

    it('home should be selected by default', async () => {
        render(
            <MemoryRouter>
                    <Navbar />
                </MemoryRouter>
                    )
        await waitFor(() =>{
                expect(screen.getByTestId('home')).toHaveClass('ant-menu-item-selected ')
                expect(screen.getByTestId('upload')).not.toHaveClass('ant-menu-item-selected ')
            }
        )
    });

    it('location should match selected menu item', async () => {
        render(
            <MemoryRouter initialEntries={['/upload']} >
                    <Navbar />
                </MemoryRouter>
        )
        await waitFor(() =>{
                expect(screen.getByTestId('home')).not.toHaveClass('ant-menu-item-selected ')
                expect(screen.getByTestId('upload')).toHaveClass('ant-menu-item-selected ')
            }
        )
    });

    it('should navigate to upload on click', async () => {
        render(
            <MemoryRouter>
                    <Navbar />
                    <Route
                        path="*"
                        render={({ location }:RouteComponentProps) =>
                            <div data-testid="location-display">{location.pathname}</div>
                        }
                    />
                </MemoryRouter>
        )
        userEvent.click(screen.getByTestId('upload'))
        await waitFor(() =>{
                expect(screen.getByTestId('location-display')).toHaveTextContent('/upload')
            }
        )
    });
})