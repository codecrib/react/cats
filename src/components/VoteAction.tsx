import React from 'react'
import {Tooltip} from 'antd'
import {MinusCircleTwoTone, PlusCircleTwoTone} from "@ant-design/icons"
import {Image} from "../types"

interface Props {
    image:Image
    vote: 'up' | 'down'
    onClick: (image:Image, value:0|1)=> void
}

export const VoteAction = ({image, vote, onClick}:Props) =>  {
    const value:0|1 = vote==='up'? 1 : 0;
    return (
        <Tooltip title={vote==='up' ? 'Vote Up' :  'Vote Down' } >
            {vote==='up' ?
                <PlusCircleTwoTone onClick={()=>onClick(image,value)}  twoToneColor={image?.vote?.value === 1 ? '#52c41a' : undefined}  /> :
                <MinusCircleTwoTone  onClick={()=>onClick(image,value)}  twoToneColor={image?.vote?.value === 0 ? '#eb2f96' : undefined} />
            }
        </Tooltip>
    )
}