import React  from 'react';
import 'antd/dist/antd.css'
import { BrowserRouter } from 'react-router-dom';
import {Provider} from "react-redux";

import {MainLayout, ErrorBoundary} from "./components";
import Routes from "./Routes";
import store from "./store";

const App = () => {
  return (
      <ErrorBoundary>
          <Provider store={store}>
              <BrowserRouter basename={process.env.PUBLIC_URL}>
                <MainLayout>
                    <Routes/>
                </MainLayout>
              </BrowserRouter>
          </Provider>
      </ErrorBoundary>
  );
}

export default App;
