import React, {ReactElement, PropsWithChildren} from 'react'
import { render as rtlRender } from '@testing-library/react'
import {applyMiddleware, createStore} from 'redux'
import { Provider } from 'react-redux'
import {rootReducer} from '../store/reducers'
import thunk from "redux-thunk";

function render(
    ui:ReactElement,
    {
      preloadedState,
      store = createStore(rootReducer, preloadedState, applyMiddleware(thunk)),
      ...renderOptions
    }:any = {}
) {
  function Wrapper({ children }: PropsWithChildren<any>) {
    return <Provider store={store}>{children}</Provider>
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

// re-export everything
export * from '@testing-library/react'
// override render method
export { render }