import { rest } from 'msw'
import config from '../config'
import {get} from 'lodash'
const API_URL = config.API_URL

const handlers = [
  rest.get(`${API_URL}/images`, (req, res, ctx) => {
    if (req.url.searchParams.get('sub_id')==='fetch_error') {
      return res(ctx.status(500), ctx.json({error:{message:'fetch error'}}))
    }
    return res(ctx.status(200), ctx.json([]))
  }),
  rest.post(`${API_URL}/favourites`,(req, res, ctx) => {
    const sub_id = get(req.body,'sub_id')
    if (sub_id==='fetch_error') {
      return res(ctx.status(500), ctx.json({error:{message:'fetch error'}}))
    }
    return res(ctx.status(200), ctx.json({id:'test_fav_id'}))
  })
  ,
  rest.get(`${API_URL}/votes`,(req, res, ctx) => {
    const page = req.url.searchParams.get('page')
    if (page==='0') {
      return res( ctx.status(200), ctx.json([{id:'test_vote_id_1', image_id:'test_id', value:1},{id:'test_vote_id_2', image_id:'test_id', value:1}]))
    }
     if (page==='1') {
       return res( ctx.status(200), ctx.json([{id:'test_vote_id_2', image_id:'test_id', value:1}]))
    }
    return res(ctx.status(500))
  })
  ,
  rest.post(`${API_URL}/votes`,(req, res, ctx) => {
    const sub_id = get(req.body,'sub_id')
    if (sub_id==='test_sub_id_fetch_error') {
      return res(ctx.status(500), ctx.json({error:{message:'fetch error'}}))
    }
    return res(ctx.status(200), ctx.json({id:'test_vote_id'}))
  })
  ,
  rest.delete(`${API_URL}/favourites/:id`,(req, res, ctx) => {
    if (req.params.id==='test_fav_id_fetch_error') {
      return res(ctx.status(500), ctx.json({error:{message:'fetch error'}}))
    }
    return res(ctx.status(200), ctx.json({status:'SUCCESS'}))
  })
  ,
  rest.get(`${API_URL}/votes`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json([]))
  })
]

export { handlers }
