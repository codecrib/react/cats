import React from 'react';
import {render, screen ,waitFor,act} from '../integration-tests';
import LandingPage from './LandingPage';

describe('<LandingPage/>', () => {
    it('should render', async () => {
        act(()=>{
            render(<LandingPage/>)
        })
        await waitFor(() =>{
                expect(screen.getByText(/Home Page/i)).toBeInTheDocument()
            }
        )
    });
})