import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { fetchAllVotes, fetchImages} from "../store/actionCreators";
import {RootState} from "../store";
import {Dispatch} from "redux";
import { Card, List, Pagination} from "antd";
import {User,Image,ImageState} from "../types";
import { ImageCard } from "../components";

function LandingPage() {
    const dispatch: Dispatch<any> = useDispatch()
    const user:User = useSelector((state: RootState) => state.user)
    const images:ImageState= useSelector((state: RootState) => state.images)

    const  [page, setPage] = useState<number>(1)
    const  [limit, setLimit] = useState<number>(8)

    useEffect(() => {
      dispatch( fetchImages(user.email,page-1, limit))
    },[dispatch, user.email, page, limit])

    useEffect(() => {
        dispatch(fetchAllVotes())
    },[dispatch])

    return (
        <Card title="Home Page"
              style={{margin:12}}
        >
            <List
                style={{margin:12}}
                grid={{
                    gutter: 12,
                    xs: 1,
                    sm: 1,
                    md: 2,
                    lg: 4,
                    xl: 4,
                    xxl: 4,
                }}
                dataSource={images.images}
                renderItem={(item:Image, index:number) => (

                    <List.Item>
                        <ImageCard image={item} index={index}/>
                    </List.Item>
                )}
            />
            <Pagination
                responsive
                pageSizeOptions={['8','16','32','64']}
                showSizeChanger
                onShowSizeChange={(current:number,size:number)=> {
                    setLimit(size)
                }}
                onChange={(page:number)=>  {
                    setPage(page)
                }}
                defaultCurrent={page}
                pageSize={limit}
                total={images.total}
            />
        </Card>
    );
}




export default LandingPage;