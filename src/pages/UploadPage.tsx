import React from 'react'
import { Upload, message ,Card} from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import {useSelector} from "react-redux";
import {RootState} from "../store";
import {useHistory} from "react-router-dom";
import {User} from "../types";
import config from "../config";

const { Dragger } = Upload;
const {API_URL, API_KEY} = config

const UploadPage = () => {
    const user:User = useSelector((state: RootState) => state.user)
    const history = useHistory();


    const props = {
        name: 'file',
        multiple: false,
        data:{"sub_id":`${user?.email}`},
        headers: {"x-api-key":`${API_KEY}`},
        action: `${API_URL}/images/upload`,
        onChange(info:any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
                history.push('/')
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed. ${info.file?.response?.message}`);
            }
        },
        onDrop(e:any) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };

    return (

            <Card title="Upload Page"
                  style={{margin:24}}
            >
                <Dragger {...props}>
                    <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">Click or drag cat photo to this area to upload</p>
                    <p className="ant-upload-hint">
                        Support for a single upload.
                    </p>
                </Dragger>
            </Card>

    );
}

export default UploadPage;