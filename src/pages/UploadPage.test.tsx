import React from 'react';
import { render, screen ,waitFor} from '../integration-tests';
import UploadPage from './UploadPage';
import store from "../store";
import {Provider} from 'react-redux'

describe('<UploadPage/>', () => {
    it('should render', async () => {
        render(   <Provider store={store}><UploadPage /></Provider>)
        await waitFor(() =>{
                expect(screen.getByText(/Upload Page/i)).toBeInTheDocument()
            }
        )
    });
})


