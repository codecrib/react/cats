import React from 'react';
import { render, screen ,waitFor} from './integration-tests';
import App from './App';


describe('<App/>', () => {
    it('should render', async () => {
        render(<App />)
        await waitFor(() =>{
                expect(screen.getByText(/Home Page/i)).toBeInTheDocument()
            }
        )
    });
})



