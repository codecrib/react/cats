export type User = {
    email:string
    name:string
    imageUrl?:string
}

export type Image = {
    id:string
    url:string
    favourite? : Favourite
    vote?:Vote
}

export type Favourite = {
    id:string
}

export type Vote = {
    id:string
    image_id?:string
    value?: 0|1
}

export type score = {
    image_id:string
    score:number
}


// reducer state types
type ImageState = {
    images: Image[]
    status: 'idle'|'loading'
    error?: any
    total?: number

}

export type ScoreState =  score[]


