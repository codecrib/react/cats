import actionTypes from "../actionTypes"
import {AnyAction} from "redux"
import {User} from "../../types";

const initialState:User= {name:'anonymous', email: 'anonymous'}

const userReducer = (state=initialState, action : AnyAction )=>{
    switch (action.type) {
        case actionTypes.LOGIN_SUCCESS :
            return action.payload
        case actionTypes.LOGIN_FAILED :
            return  initialState
        case actionTypes.LOGOUT_SUCCESS :
            return initialState
        case actionTypes.LOGOUT_FAILED :
            return state
        default:
            return state
    }
}

export default userReducer