import actionTypes from "../actionTypes";
import {AnyAction} from "redux";
import { ImageState} from "../../types";

const initialState:ImageState = {
    images: [],
    status: 'idle',
    total: 0,
    error: null
}

const imageReducer = (state:ImageState= initialState, action : AnyAction )=>{
    switch (action.type) {
        case actionTypes.FETCH_IMAGES_REQUEST :
            return { ...state , status: 'loading' , error:null}
        case actionTypes.FETCH_IMAGES_SUCCESS :
            return { ...state , status: 'idle' , ...action.payload}
        case actionTypes.FETCH_IMAGES_FAIL :
            return { ...state , status: 'idle' , ...action.payload}
        case actionTypes.POST_FAV_REQUEST :
            return { ...state , status: 'loading' , error:null}
        case actionTypes.POST_FAV_SUCCESS :
        case actionTypes.DELETE_FAV_SUCCESS :
        case actionTypes.POST_VOTE_SUCCESS :
            const newImages = [...state.images].map(image => image.id === action.payload?.id ? action.payload : image)
            return { ...state , status: 'idle', images: newImages }
        case actionTypes.POST_FAV_FAIL :
            return { ...state , status: 'idle' , ...action.payload}
        case actionTypes.DELETE_FAV_REQUEST :
            return { ...state , status: 'loading' , error:null}
        case actionTypes.DELETE_FAV_FAIL :
            return { ...state , status: 'idle' , ...action.payload}
        case actionTypes.POST_VOTE_REQUEST :
            return { ...state , status: 'loading' , error:null}
        case actionTypes.POST_VOTE_FAIL :
            return { ...state , status: 'idle' , ...action.payload}
        default:
            return state
    }
}

export default imageReducer