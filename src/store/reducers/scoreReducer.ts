import actionTypes from "../actionTypes";
import {AnyAction} from "redux";
import {ScoreState} from "../../types";

const initialState:ScoreState = []

const scoreReducer = (state:ScoreState= initialState, action : AnyAction )=>{
    switch (action.type) {
        case actionTypes.SET_SCORES:
            return action.payload
        default:
            return state
    }
}

export default scoreReducer