import {combineReducers} from 'redux'
import userReducer from "./userReducer";
import imageReducer from "./imageReducer";
import scoreReducer from "./scoreReducer";


export const rootReducer = combineReducers(
    {
        user:userReducer,
        images:imageReducer,
        scores:scoreReducer
    }
)

