import {createStore,applyMiddleware} from 'redux'
import {rootReducer} from "./reducers";
import thunk from "redux-thunk"

const initialState = {}
const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store