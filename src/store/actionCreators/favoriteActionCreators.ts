import {Image} from "../../types";
import {Dispatch} from "redux";
import axios from "axios";
import actionTypes from "../actionTypes";
import {message} from "antd";
import {axiosConfig} from "./index";
import config from "../../config";

const {API_URL} = config

export const favImage = (image:Image,sub_id:string) => {
    return function(dispatch: Dispatch<any>) {
        return axios.post(`${API_URL}/favourites`,
            {sub_id:sub_id,image_id:image.id},
            axiosConfig)
            .then(({ data }) => {
                dispatch(updateForPostFav( { ...image ,favourite:{id:data.id}}));
            }).catch((error) => {
                    dispatch(setPostFavError(error));
                }
            );
    };
}

export const updateForPostFav = (data:Image) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({type:actionTypes.POST_FAV_SUCCESS,
            payload:data});
    };
}

export const setPostFavError = (error:any) => {
    return function(dispatch: Dispatch<any>) {
        message.error(' error while adding favourite')
        dispatch({type:actionTypes.POST_FAV_FAIL,
            payload: {error}});
    };
}

export const unFavImage = (image:Image) => {
    return function(dispatch: Dispatch<any>) {
        return axios.delete(`${API_URL}/favourites/${image?.favourite?.id}`,
            axiosConfig)
            .then(() => {
                const {favourite, ...rest} = image
                dispatch(updateForDeleteFav(rest));
            }).catch((error) => {
                    dispatch(setDeleteFavError(error));
                }
            );
    };
}

export const updateForDeleteFav = (data:Image) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({type:actionTypes.DELETE_FAV_SUCCESS,
            payload:data});
    };
}

export const setDeleteFavError = (error:any) => {
    return function(dispatch: Dispatch<any>) {
        message.error(' error while removing favourite')
        dispatch({type:actionTypes.DELETE_FAV_FAIL,
            payload: {error}});
    };
}