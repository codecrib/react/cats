import {Dispatch} from "redux";
import axios from "axios";
import actionTypes from "../actionTypes";
import {message} from "antd";
import {axiosConfig} from "./index";
import config from '../../config';
import {ImageState} from "../../types";

const {API_URL} = config;

export const fetchImages = (sub_id:string,page:number=0,limit:number=100) => {
    return function(dispatch: Dispatch<any>) {
        return axios.get(`${API_URL}/images?sub_id=${sub_id}&page=${page}&limit=${limit}&include_vote=1&include_favourite=1&order=desc`,
            axiosConfig)
            .then(({ data,headers}) => {
                dispatch(setImages({images:data, total:parseInt(headers['pagination-count']||"0")}));
            }).catch((error) => {
                    dispatch(setImageFetchError({error}));
                }
            );
    };
}

export const setImages = (payload:Partial<ImageState>) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({type:actionTypes.FETCH_IMAGES_SUCCESS,
            payload:payload});
    };
}

export const setImageFetchError = (payload:Partial<ImageState>) => {
    return function(dispatch: Dispatch<any>) {
        message.error('image fetch error')
        dispatch({type:actionTypes.FETCH_IMAGES_FAIL,
            payload:payload});
    };
}