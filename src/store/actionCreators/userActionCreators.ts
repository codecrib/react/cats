import {User} from "../../types";
import {AnyAction} from "redux";
import actionTypes from "../actionTypes";
import {message} from "antd";

export const loginSuccessResponse = (user: User) => {
    const action: AnyAction = {
        type: actionTypes.LOGIN_SUCCESS,
        payload: user
    }
    message.success('login success')
    return action
}

export const loginFailureResponse = () => {
    const action: AnyAction = {
        type: actionTypes.LOGIN_FAILED
    }
    message.error('login error')
    return action
}

export const logoutSuccessResponse = () => {
    const action: AnyAction = {
        type: actionTypes.LOGOUT_SUCCESS,
    }
    message.success('logout success')
    return action
}

export const logoutFailureResponse = () => {
    const action: AnyAction = {
        type: actionTypes.LOGOUT_FAILED
    }
    message.error('logout error')
    return action
}