import {setImageFetchError, setImages, fetchImages} from './imageActionCreators';
import store from "../index";
import {waitFor} from "../../integration-tests";


describe('Image Action Creators', () => {
    it('setImageFetchError', async () => {
        store.dispatch(setImageFetchError({error:'test'}))
        await waitFor(() => {
            expect(store.getState().images.error).toEqual('test')
        })
    });

    it('setImages', async () => {
        store.dispatch(setImages({images:[{id:'test_id', url:'test_url'}]}))
        await waitFor(() => {
            expect(store.getState().images.images).toHaveLength(1)
            expect(store.getState().images.images[0].id).toEqual('test_id')
            expect(store.getState().images.images[0].url).toEqual('test_url')
        })
    });

    it('fetchImages', async () => {
        await store.dispatch(fetchImages('test_sub_id'))
        await waitFor(() =>
        expect(store.getState().images.images).toEqual([])
        )
    });

    it('fetchImages on error', async () => {
        await store.dispatch(fetchImages('fetch_error'))
        await waitFor(() =>
            expect(store.getState().images.error).toBeDefined()
        )
    });

})