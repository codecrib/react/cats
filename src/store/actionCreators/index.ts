import config from "../../config"

export const axiosConfig = {headers: {"x-api-key":config.API_KEY}}
export * from './userActionCreators'
export * from './imageActionCreators'
export * from './voteActionCreators'
export * from './favoriteActionCreators'