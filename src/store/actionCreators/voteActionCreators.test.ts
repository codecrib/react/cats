import {voteForImage,fetchAllVotes } from './voteActionCreators';
import store from "../index";
import {waitFor} from "../../integration-tests";
import {setImages} from "./imageActionCreators";


describe('Vote Action Creators', () => {

    it('voteForImage', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url'}]}))
        await store.dispatch(voteForImage({id:'test_id', url:'test_url'}, 'test_sub_id', 1))
        await waitFor(() =>{
            expect(store.getState().images.images[0].vote.id).toEqual('test_vote_id')
            expect(store.getState().images.images[0].vote.value).toEqual(1)
        })
    });

    it('voteForImage on error', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url'}]}))
        await store.dispatch(voteForImage({id:'test_id', url:'test_url'},'test_sub_id_fetch_error', 1))
        await waitFor(() =>{
           // expect(store.getState().images.error).toEqual({error:{message:'fetch error'}})
            expect(store.getState().images.images[0].vote).not.toBeDefined()
        })
    });

    it('fetchAllVotes', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url'}]}))
        await store.dispatch(fetchAllVotes())
        await waitFor(() =>{
            expect(store.getState().scores).toHaveLength(1)
            expect(store.getState().scores[0].score).toEqual(2)
        })
    });

})