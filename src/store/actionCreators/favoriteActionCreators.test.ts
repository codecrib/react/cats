import {favImage,unFavImage } from './favoriteActionCreators';
import store from "../index";
import {waitFor} from "../../integration-tests";
import {setImages} from "./imageActionCreators";


describe('Favorite Action Creators', () => {

    it('favImage', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url'}]}))
        await store.dispatch(favImage({id:'test_id', url:'test_url'}, 'test_sub_id'))
        await waitFor(() =>{
            expect(store.getState().images.images[0].favourite.id).toEqual('test_fav_id')
        })
    });

    it('favImage on error', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url'}]}))
        await store.dispatch(favImage({id:'test_id', url:'test_url'}, 'fetch_error'))
        await waitFor(() =>{
           // expect(store.getState().images.error).toEqual({error:{message:'fetch error'}})
            expect(store.getState().images.images[0].favourite).not.toBeDefined()
        })
    });

    it('unFavImage', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url', favourite:{id:'test_fav_id'}}]}))
        await store.dispatch(unFavImage({id:'test_id', url:'test_url', favourite:{id:'test_fav_id'}}))
        await waitFor(() =>{
            expect(store.getState().images.images[0].favourite).not.toBeDefined()
        })
    });

    it('unFavImage on error', async () => {
        store.dispatch( setImages({images:[{id:'test_id', url:'test_url', favourite:{id:'test_fav_id_fetch_error'}}]}))
        await store.dispatch(unFavImage({id:'test_id', url:'test_url', favourite:{id:'test_fav_id_fetch_error'}}))
        await waitFor(() =>{
            expect(store.getState().images.images[0].favourite.id).toEqual('test_fav_id_fetch_error')
          //  expect(store.getState().images.error).toEqual({error:{message:'fetch error'}})
            }
        )
    });

})