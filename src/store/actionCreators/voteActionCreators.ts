import {Image} from "../../types";
import {Dispatch} from "redux";
import axios from "axios";
import actionTypes from "../actionTypes";
import {message} from "antd";
import {axiosConfig} from "./index";
import {groupBy} from "lodash";
import config from "../../config";

const {API_URL} = config;


export const voteForImage = (image:Image,sub_id:string, value:0|1) => {
    return function(dispatch: Dispatch<any>) {
        return axios.post(`${API_URL}/votes`,
            {
                image_id: image.id,
                sub_id: sub_id,
                value: value
            },
            axiosConfig)
            .then(({ data }) => {
                dispatch(updateForPostVote( { ...image ,vote:{id:data.id, value:value}}));
                dispatch(fetchAllVotes())
            }).catch((error) => {
                    dispatch(setPostVoteError(error));
                }
            );
    };
}

export const updateForPostVote = (data:Image) => {
    return function(dispatch: Dispatch<any>) {
        dispatch({type:actionTypes.POST_VOTE_SUCCESS,
            payload:data});
    };
}

export const setPostVoteError = (error:any) => {
    return function(dispatch: Dispatch<any>) {
        message.error(' error while voting')
        dispatch({type:actionTypes.POST_VOTE_FAIL,
            payload: {error}});
    };
}

export const fetchAllVotes = (page:number=0,limit:number=100, memo:any[]=[]) => {
    return function(dispatch: Dispatch<any>) {
        return axios.get(`${API_URL}/votes?page=${page}&limit=${limit}`,
            axiosConfig)
            .then(({ data, headers }) => {
                const isLastPage = headers["pagination-count"] ? page === Math.floor(parseInt(headers["pagination-count"])/limit) : true
                if (isLastPage) {
                    dispatch(calculateScores([...memo,...data]));
                }
                else{
                    dispatch(fetchAllVotes(page+1,limit, [...memo,...data]))
                }
            })
    };
}

const calculateScores = (data:any) => {
    return function(dispatch: Dispatch<any>) {
        const scores =   Object.values(groupBy(data,'image_id')).map(x=> ({ image_id: x[0].image_id , score:x.reduce((acc,cur)=> acc + (cur.value===0 ? -1:1), 0) }))
        dispatch({type:actionTypes.SET_SCORES, payload:scores})
    }
}