import {loginSuccessResponse,loginFailureResponse,logoutSuccessResponse,logoutFailureResponse } from './userActionCreators';
import store from "../index";
import {waitFor} from "../../integration-tests";
import {User} from "../../types";

const anonymousUser:User = {name:'anonymous',email:'anonymous'}
const user:User = {name:'test_name', email:'test_email'}


describe('User Action Creators', () => {

    it('loginSuccessResponse', async () => {
        await store.dispatch(loginSuccessResponse(user))
        await waitFor(() =>{
            expect(store.getState().user).toEqual(user)
        })
    });

    it('loginFailureResponse', async () => {
        await store.dispatch(loginFailureResponse())
        await waitFor(() =>{
            expect(store.getState().user).toEqual(anonymousUser)
        })
    });

    it('logoutSuccessResponse', async () => {
        await store.dispatch(loginSuccessResponse(user))
        await store.dispatch(logoutSuccessResponse())
        await waitFor(() =>{
            expect(store.getState().user).toEqual(anonymousUser)
        })
    });

    it('logoutFailureResponse', async () => {
        await store.dispatch(loginSuccessResponse(user))
        await store.dispatch(logoutFailureResponse())
        await waitFor(() =>{
            expect(store.getState().user).toEqual(user)
        })
    });

})